import { Link } from "react-router-dom";
import { useState } from "react";
import React from "react";
import Logo from "@/components/Logo";
import useAuthContext from "@/context/AuthContext";

function Navbar() {
  // State to track the visibility of the collapsible menu
  const [menuOpen, setMenuOpen] = useState(false);

  const { user, logout } = useAuthContext();
  return (
    <nav className="flex items-center justify-between flex-wrap bg-slate-900 p-6">
      <Logo></Logo>
      <div
        className="block lg:hidden"
        onClick={() => setMenuOpen(!menuOpen)} // Toggle menuOpen state
      >
        <button className="flex items-center px-3 py-2 border rounded text-white hover:text-slate-50 hover:border-white">
          <svg
            className="fill-current h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div
        className={`w-full block flex-grow lg:flex lg:items-center lg:w-auto ${
          menuOpen ? "block" : "hidden"
        }`}
      >
        <div className="text-sm lg:flex-grow">
          <Link
            to="/"
            area-current="page"
            className="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-gray-100 mr-4"
          >
            Home
          </Link>
          {user ? (
            <button
              onClick={logout}
              className="block mt-4 lg:inline-block lg:mt-0  text-white hover:text-gray-100 mr-4"
            >
              Logout
            </button>
          ) : (
            <React.Fragment>
              <Link
                to="/register"
                className="block mt-4 lg:inline-block lg:mt-0  text-white hover:text-gray-100 mr-4"
              >
                Register
              </Link>
              <Link
                to="/login"
                className="block mt-4 lg:inline-block lg:mt-0  text-white hover:text-gray-100 mr-4"
              >
                Login
              </Link>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
