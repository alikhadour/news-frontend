import React from "react";

function Article({ index, article }) {
  return (
    <div key={index} className="w-full md:w-1/2 lg:w-1/4 xl:w-1/5 flex-shrink-0">
      <div key={index} className="max-w-sm rounded overflow-hidden shadow-md">
        <img
          src={article.url_to_image ?? 'src/assets/default-news.png'}
          alt="Article Image"
          className="w-full h-40 object-cover"
        />
        <div className="px-6 py-4">
          <div className="mb-2 text-sm text-gray-600">
            Published on {article.published_at}
          </div>
          <h2 className="text-lg font-semibold mb-2">{article.title}</h2>
          <p className="text-gray-800">{article.description}</p>
          <div className="mt-4">
            <a
              href={article.url}
              className="text-blue-500 hover:underline"
              target="_blank"
              rel="noopener noreferrer"
            >
              Read more
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Article;
