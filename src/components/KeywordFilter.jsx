import React, { useState } from "react";

const KeywordFilter = ({ onKeywordsChange }) => {
  const [newsInput, setNewsInput] = useState("");
  const [newsArray, setNewsArray] = useState([]);

  const handleInputChange = (event) => {
    setNewsInput(event.target.value);
  };

  const handleKeyPress = () => {
    if (event.key === "Enter") {
      event.preventDefault();
      if (newsInput.trim() !== "") {
        setNewsArray([...newsArray, newsInput]);
        setNewsInput("");
        onKeywordsChange([...newsArray, newsInput]); // Pass the updated array to the parent

      }
    }
  };

  const removeNewsElement = (index) => {
    const updatedNewsArray = newsArray.filter((_, i) => i !== index);
    setNewsArray(updatedNewsArray);
    onKeywordsChange(updatedNewsArray); // Pass the updated array to the parent

  };

  return (
    <>
      <input
        type="text"
        value={newsInput}
        onChange={handleInputChange}
        onKeyDown={handleKeyPress}
        placeholder="Enter keywords to search for articles..."
        className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
        />
      <ul className="mt-4 flex flex-row">
        {newsArray.map((news, index) => (
          <li key={index} className="flex items-center mb-2">
            <span className="flex-grow bg-gray-500 text-white px-3 py-1 rounded-lg mr-2">
              {news}
              &nbsp;
              <button
                onClick={() => removeNewsElement(index)}
                className="text-white hover:text-grey-100 focus:outline-none"
              >
                &#x2715;
              </button>
            </span>
          </li>
        ))}
      </ul>
    </>
  );
};

export default KeywordFilter;
