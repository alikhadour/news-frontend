import useAuthContext from "@/context/AuthContext";
import { Navigate, Outlet } from "react-router-dom";
import Navbar from "@/components/Navbar";
function AuthLayout() {
  const { user } = useAuthContext();

  return user ? (
    <>
      <Navbar></Navbar>
      <div className="max-w-7xl mx-auto mt-6">
        <Outlet />
      </div>
    </>
  ) : (
    <Navigate to="/login" />
  );
}

export default AuthLayout;
