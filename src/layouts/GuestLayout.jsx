import useAuthContext from "@/context/AuthContext";
import { Navigate, Outlet } from "react-router-dom";

function GuestLayout() {
  const { user } = useAuthContext();

  return !user ? (
    <div className="max-w-7xl mx-auto mt-6 flex flex-col justify-center items-center h-screen bg-gray-50 dark:bg-gray-900">
      <Outlet />
    </div>
  ) : (
    <Navigate to="/" />
  );
}

export default GuestLayout;
