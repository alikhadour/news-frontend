import { Route, Routes } from "react-router-dom";
import Login from "./pages/Login.jsx";
import Register from "./pages/Register.jsx";
import Home from "./pages/Home.jsx";
import AuthLayout from "./layouts/AuthLayout.jsx";
import GuestLayout from "./layouts/GuestLayout.jsx";
export default function App() {
  return (
    <>
      <Routes>
        <Route Component={GuestLayout}>
          <Route path="/login" Component={Login}></Route>
        </Route>
        <Route Component={GuestLayout}>
          <Route path="/register" Component={Register}></Route>
        </Route>
        <Route Component={AuthLayout}>
          <Route path="/" Component={Home}></Route>
        </Route>
      </Routes>
    </>
  );
}
