import KeywordFilter from "@/components/KeywordFilter";
import { useState, useEffect, useCallback } from "react";
import axios from "@/api/axios";
import _ from "lodash"; // Import lodash
import Article from "../components/Article";
import { DebounceInput } from "react-debounce-input";

const Home = () => {
  const [articles, setArticles] = useState([]);
  const [filters, setFilters] = useState({
    source: "",
    keywords: [],
    category: "",
    date: "",
  });

  const handleKeywordsChange = (keywords) => {
    handleFilterChange("keywords", keywords);
  };

  const handleFilterChange = (filterName, value) => {
    setFilters((prevFilters) => ({
      ...prevFilters,
      [filterName]: value,
    }));

    fetchData(filters)
  };

  useEffect(() => {
    // Fetch data when the component mounts
    fetchData(filters);
  }, []);

  const fetchData = async (filters) => {
    try {
      const response = await axios.post("api/articles/filter", filters);
      setArticles(response.data);
      console.log(response);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  return (
    <div className="flex flex-wrap justify-center space-y-4 p-4">
      <div className="w-full mb-4">
        <label
          htmlFor="keywords"
          className="block mb-1 text-sm font-medium text-gray-700"
        >
          Keywords:
        </label>
        <KeywordFilter onKeywordsChange={handleKeywordsChange}></KeywordFilter>
      </div>
      <div className="w-full mb-4">
        <label
          htmlFor="source"
          className="block mb-1 text-sm font-medium text-gray-700"
        >
          Source: {filters.source}
        </label>
        <DebounceInput
          type="text"
          value={filters.source}
          debounceTimeout={500}
          onChange={(e) => handleFilterChange("source", e.target.value)}
          placeholder="Source"
          className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
        />
      </div>

      <div className="w-full mb-4">
        <label
          htmlFor="category"
          className="block mb-1 text-sm font-medium text-gray-700"
        >
          Category: {filters.category}
        </label>
        <DebounceInput
          type="text"
          value={filters.category}
          debounceTimeout={500}
          onChange={(e) => handleFilterChange("category", e.target.value)}
          placeholder="Category"
          className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
        />
      </div>

      <div className="w-full mb-4">
        <label
          htmlFor="date"
          className="block mb-1 text-sm font-medium text-gray-700"
        >
          Date: {filters.date}
        </label>
        <DebounceInput
          type="date"
          id="date"
          name="date"
          debounceTimeout={500}
          value={filters.date}
          onChange={(e) => handleFilterChange("date", e.target.value)}
          className="w-full px-4 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
        />
      </div>
      <div className="w-full flex flex-wrap justify-center space-x-1 space-y-1">
        {articles.map((article, index) => (
          <Article index={index} article={article} key={index}></Article>
        ))}
      </div>
    </div>
  );
};

export default Home;
